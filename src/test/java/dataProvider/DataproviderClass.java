package dataProvider;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class DataproviderClass {
    @DataProvider(name = "CepProvider")
    public Object[][] getDataFromDataprovider(Method m) {
        if (m.getName().equalsIgnoreCase("consultaCep")) {
            return new Object[][]{
                    {"cep", "91060-900"},
                    {"logradouro", "Avenida Assis Brasil 3940"},
                    {"complemento", ""},
                    {"bairro", "S�o Sebasti�o"},
                    {"localidade", "Porto Alegre"},
                    {"uf", "RS"},
                    {"ibge", "4314902"},
                    {"gia", ""},
                    {"ddd", "51"},
                    {"siafi", "8801"},

            };
        }
        if (m.getName().equalsIgnoreCase("validaCEPInexistente")) {
            return new Object[][]{
                    {"erro", true},
            };
        }
        if (m.getName().equalsIgnoreCase("validaCEPInvalido")) {
            return new Object[][]{
                    {"html.body.h1", "Erro 400"},
                    {"html.body.h2", "Ops!"},
                    {"html.body.h3", "Verifique a sua URL (Bad Request)"},
            };
        } else{
                return new Object[][]{
                        {"cep[0]", "94085-170"},
                        {"complemento[0]", ""},
                        {"bairro[0]", "Morada do Vale I"},
                        {"localidade[0]", "Gravata�"},
                        {"uf[0]", "RS"},
                        {"ibge[0]", "4309209"},
                        {"gia[0]", ""},
                        {"ddd[0]", "51"},
                        {"siafi[0]", "8683"},
                        {"cep[1]", "94175-000"},
                        {"logradouro[1]", "Rua Almirante Barroso"},
                        {"complemento[1]", ""},
                        {"bairro[1]", "Recanto Corcunda"},
                        {"localidade[1]", "Gravata�"},
                        {"uf[1]", "RS"},
                        {"gia[1]", ""},
                        {"ddd[1]", "51"},
                        {"siafi[1]", "8683"}
                };
            }
        }
    }
