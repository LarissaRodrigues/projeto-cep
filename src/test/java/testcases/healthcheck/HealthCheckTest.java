package testcases.healthcheck;

import api.BaseTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;


public class HealthCheckTest extends BaseTest{

    @Test(groups = "healthcheck")
    public void statusUP() {
          when().
                get("91060900/json/").
                then().
                statusCode(200).
                body("status", equalTo("UP"));
    }
}
