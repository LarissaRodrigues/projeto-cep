package funcionalTeste;

import api.utils.ValidatorResponse;
import dataProvider.DataproviderClass;
import io.sicredi.tm4j.common.annotations.TestCase;
import org.testng.annotations.Test;

import static api.auxiliar.CepApi.CEP;

public class CepTest extends ValidatorResponse {

    @TestCase(key = "CONT06-T48")
    @Test (groups = "regressao", dataProvider = "CepProvider", dataProviderClass= DataproviderClass.class)
    public void consultaCep(String valor, String esperado){

        getCep(CEP);
        validaHeaders("Connection", "keep-alive");
        validaHeaders("Content-Type", "application/json; charset=utf-8");
        validaStatusCode(200);
        validaContrato("cep_schema");
        validaBody(valor, esperado);

    }


}
