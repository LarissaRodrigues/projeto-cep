package api.report;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.service.ExtentService;
import com.aventstack.extentreports.service.ExtentTestManager;
import io.restassured.response.Response;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import static api.utils.CommonUtils.getValor;

public class ReportListener implements ITestListener {

    public synchronized void onStart(ITestContext context) {
        ExtentService.getInstance().setAnalysisStrategy(AnalysisStrategy.CLASS);
    }

    public synchronized void onFinish(ITestContext context) {
        ExtentService.getInstance().flush();
    }

    public synchronized void onTestStart(ITestResult result) {
        ExtentTestManager.createMethod(result, true);
    }

    public synchronized void onTestSuccess(ITestResult result) {
        ExtentTestManager.log(result, true);
    }

    public synchronized void onTestFailure(ITestResult result) {
        ExtentTestManager.log(result, true);
    }

    public synchronized void onTestSkipped(ITestResult result) {
        ExtentTestManager.log(result, true);
    }

    public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        ExtentTestManager.log(result, true);
    }

    public static synchronized void log(String txt) {
        ExtentTestManager.getTest().log(Status.INFO, txt);
    }

    public static synchronized void log(String txt, Response response) {
        ExtentTestManager.getTest().log(Status.INFO, txt + response.statusCode() +
                                                                "<hr />" + response.print());
    }
}
