package api.utils;

import api.BaseTest;
import api.tasks.CepTask;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidationException;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.Assert;

import java.io.File;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ValidatorResponse extends CepTask {

    public void validaHeaders(String campo, String retorno) {
        response.then().
                headers(campo, Matchers.equalTo(retorno)).
                contentType(ContentType.JSON);
    }

    public void validaStatusCode(int statusCode) {
        assertEquals(response.getStatusCode(), statusCode);
    }

    public void validaContrato(String json) {
        verificaArquivoExistente(json);
        try {
            response.then().assertThat().body(matchesJsonSchema(
                    new File("src/test/resources/json_schemas/cep/" + json + ".json")));
        } catch (AssertionError e) {
            System.out.println("DEU MERDA!");
            assertTrue(false);

        } catch (JsonSchemaValidationException e) {
            System.out.println(e.getMessage());
            assertTrue(false);
        }
    }

    private void verificaArquivoExistente(String json) {

        File file = new File("src/test/resources/json_schemas/cep/" + json + ".json");
        if (file.exists()) {
            assertTrue(true);
        } else {
            assertTrue(false);
        }
    }

    public void validaBody(String valor, String esperado) {
        Assert.assertEquals(response.jsonPath().getString(valor), esperado);
    }
}

