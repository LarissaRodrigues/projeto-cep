package api.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class CommonUtils {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String AMBIENTE = "uat";

    private CommonUtils() {}

    /**
     * Retorna o valor da propriedade contida no arquivo conf/config.properties
     * @param propriedade uma propriedade existente no arquivo config/config.properties
     * @return o valor da propriedade informada
     */
    public static String getValor(String propriedade) {
        Properties properties;

        try {
            properties = new Properties();
            String env = null == System.getProperty("env") ? AMBIENTE : System.getProperty("env");
            String separadorArquivo = System.getProperty("file.separator");

            try (InputStream propFileInpStream = CommonUtils.class.getClassLoader()
                    .getResourceAsStream("conf" + separadorArquivo + env + separadorArquivo + "config.properties")) {
                properties.load(Objects.requireNonNull(propFileInpStream));
            }

            return properties.getProperty(propriedade);
        } catch (IOException | NullPointerException e) {
            LOGGER.error("Propriedade {} não foi encontrada nos arquivos de configuração", propriedade, e);
        }
        return null;
    }
}
