package api;

import api.report.ReportListener;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.sicredi.tm4j.testng.TM4JTestNGListener;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import java.nio.charset.StandardCharsets;

import static api.utils.CommonUtils.getValor;
import static io.restassured.RestAssured.*;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;


@Listeners({ReportListener.class, TM4JTestNGListener.class})
public abstract class BaseTest {

    @BeforeClass(groups = {"healthcheck","regressao"})
    public synchronized void preCondicao(){
        config = newConfig().encoderConfig(encoderConfig().defaultContentCharset(StandardCharsets.UTF_8));
        baseURI = "https://viacep.com.br/";
        basePath = "ws/";
        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}