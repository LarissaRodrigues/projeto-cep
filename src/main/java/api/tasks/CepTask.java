package api.tasks;

import api.BaseTest;
import api.report.ReportListener;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static api.utils.CommonUtils.getValor;
import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.given;


public class CepTask extends BaseTest {

    public static Response response;

    public static Response getCep(String cep) {
        ReportListener.log("> Solicita Lista de Produtos e Serviços da api " + cep);

        response = given().
                pathParam("cep", cep).
                when().
                log().all().
                get("{cep}/json/")
                .then().extract().response();

        ReportListener.log("< Resposta Solicitacao Lista de Produtos e Serviços da api " + cep + ": ", response);
        return response;
    }


}
